# #!/bin/zsh


# CURRENT_DIR=$(pwd)
# echo $CURRENT_DIR
# for x in $(find ${CURRENT_DIR}/trip_protos/* -type d); do
#     sudo protoc -I = ${x} - I=${CURRENT_DIR}/trip_protos -I /usr/local/include --go_out=${CURRENT_DIR}\
#     --go-grpc_out=${CURRENT_DIR} ${trip_protos}/* .proto 
# done


#!/bin/zsh

CURRENT_DIR=$(pwd)
echo "Current directory: $CURRENT_DIR"

# Loop over proto files
for file in $(find "${CURRENT_DIR}/trip_protos" -name "*.proto"); do
    # Compile the proto file
    protoc -I"${CURRENT_DIR}/trip_protos" -I"${CURRENT_DIR}" -I/usr/local/include --go_out="${CURRENT_DIR}" --go-grpc_out="${CURRENT_DIR}" "${file}"
done
