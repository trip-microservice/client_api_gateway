CURRENT_DIR=$(shell pwd)

run:
	go run cmd/main.go

gen-proto:
	./scripts/gen_proto.sh

swag-init:
	swag init -g api/router.go -o api/docs